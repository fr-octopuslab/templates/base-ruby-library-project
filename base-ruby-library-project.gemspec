#--
# Copyright (C) 2021 - Octopus Lab SAS
# All rights are described by the GPLv3 license present in the file /LICENSE available in the
# original repository of this file or [here](https://www.gnu.org/licenses/gpl-3.0.fr.html).
#++
# frozen_string_literal: true

require_relative 'lib/my_project/version'

::Gem::Specification.new do |s|
  s.name        = 'base-ruby-library-project'
  s.version     = ::MyProject::VERSION
  s.summary     = 'Base Ruby Library Project'
  s.description = 'This is an example of ruby library with gemspec and a working .gitlab-ci.yml' \
                  "deploying doc to Gitlab's Pages"
  s.authors     = ['Roland Laurès']
  s.email       = 'roland@octopuslab.fr'
  s.files       = ::Dir.glob('{bin,lib}/**/*') + %w[LICENSE README.md]
  # s.executables = ...
  s.homepage    = 'https://rubygems.org/gems/base-ruby-library-project'
  s.require_path = 'lib'
  s.license = 'GPL v3'
  s.required_ruby_version = '>= 2.7'

  s.metadata = {
    'bug_tracker_uri' => 'https://gitlab.com/octopuslab-public/base-ruby-library-project/-/issues',
    'changelog_uri' => 'https://gitlab.com/octopuslab-public/base-ruby-library-project/-/blob/main/CHANGELOG.md',
    'documentation_uri' => "https://octopuslab-public.gitlab.io/base-ruby-library-project/#{::MyProject::VERSION}",
    'homepage_uri' => 'https://gitlab.com/octopuslab-public/base-ruby-library-project'
    # "mailing_list_uri"  => "https://groups.example.com/bestgemever",
    # "source_code_uri"   => "https://example.com/user/bestgemever",
    # "wiki_uri"          => "https://example.com/user/bestgemever/wiki",
    # "funding_uri"       => "https://example.com/donate"
  }

  # dependencies
  # s.add_dependency('dry-struct', '~> 1.4.0')

  # development dependencies
  s.add_development_dependency('pry', '~> 0.14')
  s.add_development_dependency('rspec', '~> 3.10')
  # s.add_development_dependency('rspec-dry-struct', '~> 0.5.0')
  s.add_development_dependency('rubocop', '~> 1.20.0')
  s.add_development_dependency('rubocop-rspec', '~> 2.4')
  s.add_development_dependency('simplecov', '~> 0.21.2')
  s.add_development_dependency('yard', '~> 0.9')
end
