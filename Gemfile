#--
# Copyright (C) 2021 - Octopus Lab SAS
# All rights are described by the GPLv3 license present in the file /LICENSE available in the
# original repository of this file or [here](https://www.gnu.org/licenses/gpl-3.0.fr.html).
#++
# frozen_string_literal: true

source 'https://rubygems.org'

gemspec

group :development, :test do
  # in-code debugging library
  gem 'pry', '~> 0.14'
  # Test suite we use
  gem 'rspec', '~> 3.10'
  # Test suite's extension for dry::struct library
  # gem 'rspec-dry-struct', '~> 0.5.0'
  # Guide style checker gem
  gem 'rubocop', '~> 1.20.0'
  # Guide style checker gem
  gem 'rubocop-rspec', '~> 2.4'
  # Test suite coverage library
  gem 'simplecov', '~> 0.21.2'
  # Documentation generator
  gem 'yard', '~> 0.9'
end
